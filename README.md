##Data Science Dockerfiles

This repository contains dockerfiles for easy installation of various analytics libraries and software packages.

All dockerfiles assume the base image has been sucessfully built the host machine.

In spark:
To start spark cluster with one worker:
docker-compose up --scale spark-worker=1

To connect to running spark cluster:
docker run -it --rm --network spark\_spark-network dsbase-spark:0.0.1 /bin/bash

Num. cores available for each worker is contained in docker-compose.yml
-SPARK\_NUM\_CORES
