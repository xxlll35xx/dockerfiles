#!/bin/bash

# start mariadb container in background (daemon)
docker run -d -p 3306:3306 --name my-mysql -e MYSQL_ROOT_PASSWORD=supersecret my-mysql
