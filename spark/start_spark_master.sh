#!/bin/bash

/spark/spark-2.4.3-bin-hadoop2.7/bin/spark-class org.apache.spark.deploy.master.Master \
	--ip $SPARK_LOCAL_IP \
	--port $SPARK_MASTER_PORT \
	--webui-port $SPARK_MASTER_WEBUI_PORT
