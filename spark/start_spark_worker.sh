#!/bin/bash

/spark/spark-2.4.3-bin-hadoop2.7/bin/spark-class org.apache.spark.deploy.worker.Worker \
	--webui-port $SPARK_WORKER_WEBUI_PORT --cores $SPARK_NUM_CORES $SPARK_MASTER
