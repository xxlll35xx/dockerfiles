#!/bin/bash

docker run -it --rm --name spark-master --hostname spark-master -p 7077:7077 -p 8080:8080 --network spark_network dsbase-spark:0.0.1 /bin/bash
