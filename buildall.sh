#!/bin/bash

# build all docker images
# usage:
# bash buildall.sh VERSION


VERSION=$1
if [ -z "$1" ]
	then
	echo "No version supplied"
	echo "Usage:"
	echo "bash buildall.sh VERSION"
	echo ""
	exit 1
fi
declare -a BUILDS=('dsbase' 'xgboost' 'vowpal_wabbit' 'h2o' 'python3' 'spark')

for build in "${BUILDS[@]}";
do
	echo "Running: docker build -t $build:$VERSION --force-rm $build/"
	docker build -t $build:$VERSION --force-rm $build/
done

